<?php
	$connect = mysqli_connect("localhost", "root", "", "guest_book") or die ("Ошибка: " . mysqli_connect_errno($con));
	
	if (isset($_POST['btn'])) {
		$name = mysqli_real_escape_string($connect, $_POST['name']);
		$email = mysqli_real_escape_string($connect, $_POST['email']);
		$text = mysqli_real_escape_string($connect, $_POST['text']);
		
		if (empty($name) || empty($email) || empty($text)) {
			echo "Не все поля заполнены";
		}
		else {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				echo "Неправильно введена почта";
			}
			else {
				if (!mysqli_query($connect, "INSERT INTO messages(name, email, text) VALUE ('{$name}', '{$email}', '{$text}');")) {
					echo "Произошла ошибка отправки сообщения";
				}
				else {
					unset($_POST);
					header('Location:' . $_SERVER['PHP_SELF']);
					exit();
				}
			}
		}
	}
	
	if (isset($_GET['del'])) {
		$id = mysqli_real_escape_string($connect, $_GET['del']);
		if (mysqli_query($connect, "DELETE FROM messages WHERE id = {$id};")) {
			unset($_GET);
			header('Location:' . $_SERVER['PHP_SELF']);
			exit();
		}
	}
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Гостевая книга</title>
		<style>
			input, textarea {
				width: 200px;
				font-size: 14px;
				margin-bottom: 5px;
			}
			
			.msg {
				border: 2px solid;
				margin-bottom: 5px;
				padding: 2px;
				word-wrap: break-word;
			}
			
			a {
				text-decoration: none;
			}
			
			span {
				font-weight: bold;
			}
		</style>
	</head>
	<body>
		<form method="POST">
			<input type="text" placeholder="Имя" value="<?= $_POST['name']; ?>" name="name" required /> </br>
			<input type="email" placeholder="E-mail" value="<?= $_POST['email']; ?>" name="email" required /> </br>
			<textarea placeholder="Текст сообщения" name="text" rows=3 required ><?= $_POST['text']; ?></textarea> </br>
			<input type="submit" name="btn" value="Отправить"/>
		</form>
		
		<hr>
		
		<div style="width: 100%;">
			<h1>Сообщения:</h1>
			<?php
				$result = mysqli_query($connect, "SELECT COUNT(*) AS count FROM messages;");
				$posts = mysqli_fetch_array($result);
				
				if (!empty($posts['count'])) {
					$num = 5;
					$page = intval($_GET['page']);
					$total = ceil($posts['count'] / $num);
					
					if(empty($page) || $page < 0) { $page = 1; }
					elseif ($page > $total) { $page = $total; }
					
					$start = $page * $num - $num;
					$query = mysqli_query($connect, "SELECT * FROM messages ORDER BY id DESC LIMIT $start, $num;");
				
					while ($row = mysqli_fetch_array($query)) {
						echo <<<html
							<div class="msg">
								<a href="?del={$row['id']}" style="float: right;">Удалить сообщение</a>
								{$row['name']} ({$row['email']}):
								<br>
								{$row['text']}
							</div>
html;
					}
					
					for ($i = 1; $i <= $total; $i++) {
						if ($i != $page) {
							echo "<a href='?page={$i}'>{$i}</a> | ";
						}
						else {
							echo "<span>{$i}</span> | ";
						}
					}
				}
				else {
					echo "Сообщений нет";
				}
			?>
		</div>
	</body>
</html>