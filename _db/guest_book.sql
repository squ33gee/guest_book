-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 05 2019 г., 14:29
-- Версия сервера: 5.6.43
-- Версия PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `guest_book`
--
CREATE DATABASE IF NOT EXISTS `guest_book` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `guest_book`;

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `text`) VALUES
(1, 'Петр', 'petr@mail.ru', 'Сообщение 1'),
(2, 'Илья', 'ilya@gmail.com', 'Сообщение 2'),
(3, 'Лилия', 'lilya@mail.com', 'Сообщение 3'),
(4, 'Артем', 'artem@ya.ru', 'Сообщение 4'),
(5, 'Павел', 'pavel@gmail.com', 'Сообщение 5'),
(6, 'Алексей', 'lesha@tube.my', 'Сообщение 6'),
(7, 'Вадим', 'vadim@sas.com', 'Сообщение Сообщение Сообщение Сообщение Сообщение Сообщение Сообщение Сообщение Сообщение Сообщение Сообщение Сообщение Сообщение'),
(8, 'Диана', 'diana@mail.ru', 'Сообщение 8'),
(9, 'Дмитрий', 'dima@mail.ru', 'Сообщение 9'),
(10, 'Андрей', 'andrey@yandex.ru', 'Сообщение 10'),
(11, 'Ренат', 'renat@has.u', 'Сообщение 11'),
(12, 'Саша', 'sasha@io.ru', 'Сообщение 12'),
(13, 'Патрик', 'patrik@oc.tu', 'Сообщение 13'),
(14, 'Витя', 'vitya@ya.ru', 'Сообщение 14'),
(15, 'Зоя', 'zoya@mail.com', 'Сообщение 15'),
(16, 'Ростик', 'rostya@mail.ru', 'Сообщение 16');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
